package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ListeCourseTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListeCourse.class);
        ListeCourse listeCourse1 = new ListeCourse();
        listeCourse1.setId(1L);
        ListeCourse listeCourse2 = new ListeCourse();
        listeCourse2.setId(listeCourse1.getId());
        assertThat(listeCourse1).isEqualTo(listeCourse2);
        listeCourse2.setId(2L);
        assertThat(listeCourse1).isNotEqualTo(listeCourse2);
        listeCourse1.setId(null);
        assertThat(listeCourse1).isNotEqualTo(listeCourse2);
    }
}
