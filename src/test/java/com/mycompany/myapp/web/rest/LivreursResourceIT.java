package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.Livreurs;
import com.mycompany.myapp.domain.Societaire;
import com.mycompany.myapp.repository.LivreursRepository;
import com.mycompany.myapp.service.dto.LivreursDTO;
import com.mycompany.myapp.service.mapper.LivreursMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LivreursResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LivreursResourceIT {

    private static final String DEFAULT_FIRSTNAME = "Jobfskt8";
    private static final String UPDATED_FIRSTNAME = "Ywtec1";

    private static final String DEFAULT_LAST_NAME = "Taeeot0";
    private static final String UPDATED_LAST_NAME = "Jjtbrb5";

    private static final String DEFAULT_VEHICLE = "AAAAAAAAAA";
    private static final String UPDATED_VEHICLE = "BBBBBBBBBB";

    private static final Long DEFAULT_AGE = 18L;
    private static final Long UPDATED_AGE = 19L;

    private static final String ENTITY_API_URL = "/api/livreurs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LivreursRepository livreursRepository;

    @Autowired
    private LivreursMapper livreursMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLivreursMockMvc;

    private Livreurs livreurs;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Livreurs createEntity(EntityManager em) {
        Livreurs livreurs = new Livreurs()
            .firstname(DEFAULT_FIRSTNAME)
            .lastName(DEFAULT_LAST_NAME)
            .vehicle(DEFAULT_VEHICLE)
            .age(DEFAULT_AGE);
        // Add required entity
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            client = ClientResourceIT.createEntity(em);
            em.persist(client);
            em.flush();
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        livreurs.setClientfirstname(client);
        // Add required entity
        Societaire societaire;
        if (TestUtil.findAll(em, Societaire.class).isEmpty()) {
            societaire = SocietaireResourceIT.createEntity(em);
            em.persist(societaire);
            em.flush();
        } else {
            societaire = TestUtil.findAll(em, Societaire.class).get(0);
        }
        livreurs.setSocfirstname(societaire);
        return livreurs;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Livreurs createUpdatedEntity(EntityManager em) {
        Livreurs livreurs = new Livreurs()
            .firstname(UPDATED_FIRSTNAME)
            .lastName(UPDATED_LAST_NAME)
            .vehicle(UPDATED_VEHICLE)
            .age(UPDATED_AGE);
        // Add required entity
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            client = ClientResourceIT.createUpdatedEntity(em);
            em.persist(client);
            em.flush();
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        livreurs.setClientfirstname(client);
        // Add required entity
        Societaire societaire;
        if (TestUtil.findAll(em, Societaire.class).isEmpty()) {
            societaire = SocietaireResourceIT.createUpdatedEntity(em);
            em.persist(societaire);
            em.flush();
        } else {
            societaire = TestUtil.findAll(em, Societaire.class).get(0);
        }
        livreurs.setSocfirstname(societaire);
        return livreurs;
    }

    @BeforeEach
    public void initTest() {
        livreurs = createEntity(em);
    }

    @Test
    @Transactional
    void createLivreurs() throws Exception {
        int databaseSizeBeforeCreate = livreursRepository.findAll().size();
        // Create the Livreurs
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);
        restLivreursMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(livreursDTO)))
            .andExpect(status().isCreated());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeCreate + 1);
        Livreurs testLivreurs = livreursList.get(livreursList.size() - 1);
        assertThat(testLivreurs.getFirstname()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(testLivreurs.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testLivreurs.getVehicle()).isEqualTo(DEFAULT_VEHICLE);
        assertThat(testLivreurs.getAge()).isEqualTo(DEFAULT_AGE);
    }

    @Test
    @Transactional
    void createLivreursWithExistingId() throws Exception {
        // Create the Livreurs with an existing ID
        livreurs.setId(1L);
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        int databaseSizeBeforeCreate = livreursRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLivreursMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(livreursDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkFirstnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = livreursRepository.findAll().size();
        // set the field null
        livreurs.setFirstname(null);

        // Create the Livreurs, which fails.
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        restLivreursMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(livreursDTO)))
            .andExpect(status().isBadRequest());

        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkVehicleIsRequired() throws Exception {
        int databaseSizeBeforeTest = livreursRepository.findAll().size();
        // set the field null
        livreurs.setVehicle(null);

        // Create the Livreurs, which fails.
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        restLivreursMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(livreursDTO)))
            .andExpect(status().isBadRequest());

        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAgeIsRequired() throws Exception {
        int databaseSizeBeforeTest = livreursRepository.findAll().size();
        // set the field null
        livreurs.setAge(null);

        // Create the Livreurs, which fails.
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        restLivreursMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(livreursDTO)))
            .andExpect(status().isBadRequest());

        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllLivreurs() throws Exception {
        // Initialize the database
        livreursRepository.saveAndFlush(livreurs);

        // Get all the livreursList
        restLivreursMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(livreurs.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].vehicle").value(hasItem(DEFAULT_VEHICLE)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE.intValue())));
    }

    @Test
    @Transactional
    void getLivreurs() throws Exception {
        // Initialize the database
        livreursRepository.saveAndFlush(livreurs);

        // Get the livreurs
        restLivreursMockMvc
            .perform(get(ENTITY_API_URL_ID, livreurs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(livreurs.getId().intValue()))
            .andExpect(jsonPath("$.firstname").value(DEFAULT_FIRSTNAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.vehicle").value(DEFAULT_VEHICLE))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingLivreurs() throws Exception {
        // Get the livreurs
        restLivreursMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingLivreurs() throws Exception {
        // Initialize the database
        livreursRepository.saveAndFlush(livreurs);

        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();

        // Update the livreurs
        Livreurs updatedLivreurs = livreursRepository.findById(livreurs.getId()).get();
        // Disconnect from session so that the updates on updatedLivreurs are not directly saved in db
        em.detach(updatedLivreurs);
        updatedLivreurs.firstname(UPDATED_FIRSTNAME).lastName(UPDATED_LAST_NAME).vehicle(UPDATED_VEHICLE).age(UPDATED_AGE);
        LivreursDTO livreursDTO = livreursMapper.toDto(updatedLivreurs);

        restLivreursMockMvc
            .perform(
                put(ENTITY_API_URL_ID, livreursDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(livreursDTO))
            )
            .andExpect(status().isOk());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
        Livreurs testLivreurs = livreursList.get(livreursList.size() - 1);
        assertThat(testLivreurs.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testLivreurs.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testLivreurs.getVehicle()).isEqualTo(UPDATED_VEHICLE);
        assertThat(testLivreurs.getAge()).isEqualTo(UPDATED_AGE);
    }

    @Test
    @Transactional
    void putNonExistingLivreurs() throws Exception {
        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();
        livreurs.setId(count.incrementAndGet());

        // Create the Livreurs
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLivreursMockMvc
            .perform(
                put(ENTITY_API_URL_ID, livreursDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(livreursDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLivreurs() throws Exception {
        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();
        livreurs.setId(count.incrementAndGet());

        // Create the Livreurs
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLivreursMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(livreursDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLivreurs() throws Exception {
        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();
        livreurs.setId(count.incrementAndGet());

        // Create the Livreurs
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLivreursMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(livreursDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLivreursWithPatch() throws Exception {
        // Initialize the database
        livreursRepository.saveAndFlush(livreurs);

        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();

        // Update the livreurs using partial update
        Livreurs partialUpdatedLivreurs = new Livreurs();
        partialUpdatedLivreurs.setId(livreurs.getId());

        partialUpdatedLivreurs.firstname(UPDATED_FIRSTNAME).lastName(UPDATED_LAST_NAME).vehicle(UPDATED_VEHICLE);

        restLivreursMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLivreurs.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLivreurs))
            )
            .andExpect(status().isOk());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
        Livreurs testLivreurs = livreursList.get(livreursList.size() - 1);
        assertThat(testLivreurs.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testLivreurs.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testLivreurs.getVehicle()).isEqualTo(UPDATED_VEHICLE);
        assertThat(testLivreurs.getAge()).isEqualTo(DEFAULT_AGE);
    }

    @Test
    @Transactional
    void fullUpdateLivreursWithPatch() throws Exception {
        // Initialize the database
        livreursRepository.saveAndFlush(livreurs);

        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();

        // Update the livreurs using partial update
        Livreurs partialUpdatedLivreurs = new Livreurs();
        partialUpdatedLivreurs.setId(livreurs.getId());

        partialUpdatedLivreurs.firstname(UPDATED_FIRSTNAME).lastName(UPDATED_LAST_NAME).vehicle(UPDATED_VEHICLE).age(UPDATED_AGE);

        restLivreursMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLivreurs.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLivreurs))
            )
            .andExpect(status().isOk());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
        Livreurs testLivreurs = livreursList.get(livreursList.size() - 1);
        assertThat(testLivreurs.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testLivreurs.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testLivreurs.getVehicle()).isEqualTo(UPDATED_VEHICLE);
        assertThat(testLivreurs.getAge()).isEqualTo(UPDATED_AGE);
    }

    @Test
    @Transactional
    void patchNonExistingLivreurs() throws Exception {
        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();
        livreurs.setId(count.incrementAndGet());

        // Create the Livreurs
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLivreursMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, livreursDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(livreursDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLivreurs() throws Exception {
        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();
        livreurs.setId(count.incrementAndGet());

        // Create the Livreurs
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLivreursMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(livreursDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLivreurs() throws Exception {
        int databaseSizeBeforeUpdate = livreursRepository.findAll().size();
        livreurs.setId(count.incrementAndGet());

        // Create the Livreurs
        LivreursDTO livreursDTO = livreursMapper.toDto(livreurs);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLivreursMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(livreursDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Livreurs in the database
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLivreurs() throws Exception {
        // Initialize the database
        livreursRepository.saveAndFlush(livreurs);

        int databaseSizeBeforeDelete = livreursRepository.findAll().size();

        // Delete the livreurs
        restLivreursMockMvc
            .perform(delete(ENTITY_API_URL_ID, livreurs.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Livreurs> livreursList = livreursRepository.findAll();
        assertThat(livreursList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
