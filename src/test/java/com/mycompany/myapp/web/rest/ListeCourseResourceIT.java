package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.ListeCourse;
import com.mycompany.myapp.domain.enumeration.Status;
import com.mycompany.myapp.repository.ListeCourseRepository;
import com.mycompany.myapp.service.dto.ListeCourseDTO;
import com.mycompany.myapp.service.mapper.ListeCourseMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ListeCourseResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ListeCourseResourceIT {

    private static final String DEFAULT_NAME = "Rr9";
    private static final String UPDATED_NAME = "Mca9";

    private static final LocalDate DEFAULT_CREATIONDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATIONDATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_MODIFICATIONDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFICATIONDATE = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_TOTAL_ITEMS = 1L;
    private static final Long UPDATED_TOTAL_ITEMS = 2L;

    private static final Status DEFAULT_STATUS = Status.COMPLETED;
    private static final Status UPDATED_STATUS = Status.PROCESSING;

    private static final String ENTITY_API_URL = "/api/liste-courses";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ListeCourseRepository listeCourseRepository;

    @Autowired
    private ListeCourseMapper listeCourseMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restListeCourseMockMvc;

    private ListeCourse listeCourse;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListeCourse createEntity(EntityManager em) {
        ListeCourse listeCourse = new ListeCourse()
            .name(DEFAULT_NAME)
            .creationdate(DEFAULT_CREATIONDATE)
            .modificationdate(DEFAULT_MODIFICATIONDATE)
            .totalItems(DEFAULT_TOTAL_ITEMS)
            .status(DEFAULT_STATUS);
        // Add required entity
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            client = ClientResourceIT.createEntity(em);
            em.persist(client);
            em.flush();
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        listeCourse.setListname(client);
        return listeCourse;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListeCourse createUpdatedEntity(EntityManager em) {
        ListeCourse listeCourse = new ListeCourse()
            .name(UPDATED_NAME)
            .creationdate(UPDATED_CREATIONDATE)
            .modificationdate(UPDATED_MODIFICATIONDATE)
            .totalItems(UPDATED_TOTAL_ITEMS)
            .status(UPDATED_STATUS);
        // Add required entity
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            client = ClientResourceIT.createUpdatedEntity(em);
            em.persist(client);
            em.flush();
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        listeCourse.setListname(client);
        return listeCourse;
    }

    @BeforeEach
    public void initTest() {
        listeCourse = createEntity(em);
    }

    @Test
    @Transactional
    void createListeCourse() throws Exception {
        int databaseSizeBeforeCreate = listeCourseRepository.findAll().size();
        // Create the ListeCourse
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);
        restListeCourseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeCreate + 1);
        ListeCourse testListeCourse = listeCourseList.get(listeCourseList.size() - 1);
        assertThat(testListeCourse.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testListeCourse.getCreationdate()).isEqualTo(DEFAULT_CREATIONDATE);
        assertThat(testListeCourse.getModificationdate()).isEqualTo(DEFAULT_MODIFICATIONDATE);
        assertThat(testListeCourse.getTotalItems()).isEqualTo(DEFAULT_TOTAL_ITEMS);
        assertThat(testListeCourse.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createListeCourseWithExistingId() throws Exception {
        // Create the ListeCourse with an existing ID
        listeCourse.setId(1L);
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        int databaseSizeBeforeCreate = listeCourseRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restListeCourseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = listeCourseRepository.findAll().size();
        // set the field null
        listeCourse.setName(null);

        // Create the ListeCourse, which fails.
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        restListeCourseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreationdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = listeCourseRepository.findAll().size();
        // set the field null
        listeCourse.setCreationdate(null);

        // Create the ListeCourse, which fails.
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        restListeCourseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkModificationdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = listeCourseRepository.findAll().size();
        // set the field null
        listeCourse.setModificationdate(null);

        // Create the ListeCourse, which fails.
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        restListeCourseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTotalItemsIsRequired() throws Exception {
        int databaseSizeBeforeTest = listeCourseRepository.findAll().size();
        // set the field null
        listeCourse.setTotalItems(null);

        // Create the ListeCourse, which fails.
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        restListeCourseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = listeCourseRepository.findAll().size();
        // set the field null
        listeCourse.setStatus(null);

        // Create the ListeCourse, which fails.
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        restListeCourseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllListeCourses() throws Exception {
        // Initialize the database
        listeCourseRepository.saveAndFlush(listeCourse);

        // Get all the listeCourseList
        restListeCourseMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listeCourse.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].creationdate").value(hasItem(DEFAULT_CREATIONDATE.toString())))
            .andExpect(jsonPath("$.[*].modificationdate").value(hasItem(DEFAULT_MODIFICATIONDATE.toString())))
            .andExpect(jsonPath("$.[*].totalItems").value(hasItem(DEFAULT_TOTAL_ITEMS.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    void getListeCourse() throws Exception {
        // Initialize the database
        listeCourseRepository.saveAndFlush(listeCourse);

        // Get the listeCourse
        restListeCourseMockMvc
            .perform(get(ENTITY_API_URL_ID, listeCourse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(listeCourse.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.creationdate").value(DEFAULT_CREATIONDATE.toString()))
            .andExpect(jsonPath("$.modificationdate").value(DEFAULT_MODIFICATIONDATE.toString()))
            .andExpect(jsonPath("$.totalItems").value(DEFAULT_TOTAL_ITEMS.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    void getNonExistingListeCourse() throws Exception {
        // Get the listeCourse
        restListeCourseMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingListeCourse() throws Exception {
        // Initialize the database
        listeCourseRepository.saveAndFlush(listeCourse);

        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();

        // Update the listeCourse
        ListeCourse updatedListeCourse = listeCourseRepository.findById(listeCourse.getId()).get();
        // Disconnect from session so that the updates on updatedListeCourse are not directly saved in db
        em.detach(updatedListeCourse);
        updatedListeCourse
            .name(UPDATED_NAME)
            .creationdate(UPDATED_CREATIONDATE)
            .modificationdate(UPDATED_MODIFICATIONDATE)
            .totalItems(UPDATED_TOTAL_ITEMS)
            .status(UPDATED_STATUS);
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(updatedListeCourse);

        restListeCourseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, listeCourseDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isOk());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
        ListeCourse testListeCourse = listeCourseList.get(listeCourseList.size() - 1);
        assertThat(testListeCourse.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testListeCourse.getCreationdate()).isEqualTo(UPDATED_CREATIONDATE);
        assertThat(testListeCourse.getModificationdate()).isEqualTo(UPDATED_MODIFICATIONDATE);
        assertThat(testListeCourse.getTotalItems()).isEqualTo(UPDATED_TOTAL_ITEMS);
        assertThat(testListeCourse.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingListeCourse() throws Exception {
        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();
        listeCourse.setId(count.incrementAndGet());

        // Create the ListeCourse
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListeCourseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, listeCourseDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchListeCourse() throws Exception {
        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();
        listeCourse.setId(count.incrementAndGet());

        // Create the ListeCourse
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListeCourseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamListeCourse() throws Exception {
        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();
        listeCourse.setId(count.incrementAndGet());

        // Create the ListeCourse
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListeCourseMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCourseDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateListeCourseWithPatch() throws Exception {
        // Initialize the database
        listeCourseRepository.saveAndFlush(listeCourse);

        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();

        // Update the listeCourse using partial update
        ListeCourse partialUpdatedListeCourse = new ListeCourse();
        partialUpdatedListeCourse.setId(listeCourse.getId());

        partialUpdatedListeCourse.modificationdate(UPDATED_MODIFICATIONDATE).status(UPDATED_STATUS);

        restListeCourseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedListeCourse.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedListeCourse))
            )
            .andExpect(status().isOk());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
        ListeCourse testListeCourse = listeCourseList.get(listeCourseList.size() - 1);
        assertThat(testListeCourse.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testListeCourse.getCreationdate()).isEqualTo(DEFAULT_CREATIONDATE);
        assertThat(testListeCourse.getModificationdate()).isEqualTo(UPDATED_MODIFICATIONDATE);
        assertThat(testListeCourse.getTotalItems()).isEqualTo(DEFAULT_TOTAL_ITEMS);
        assertThat(testListeCourse.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateListeCourseWithPatch() throws Exception {
        // Initialize the database
        listeCourseRepository.saveAndFlush(listeCourse);

        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();

        // Update the listeCourse using partial update
        ListeCourse partialUpdatedListeCourse = new ListeCourse();
        partialUpdatedListeCourse.setId(listeCourse.getId());

        partialUpdatedListeCourse
            .name(UPDATED_NAME)
            .creationdate(UPDATED_CREATIONDATE)
            .modificationdate(UPDATED_MODIFICATIONDATE)
            .totalItems(UPDATED_TOTAL_ITEMS)
            .status(UPDATED_STATUS);

        restListeCourseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedListeCourse.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedListeCourse))
            )
            .andExpect(status().isOk());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
        ListeCourse testListeCourse = listeCourseList.get(listeCourseList.size() - 1);
        assertThat(testListeCourse.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testListeCourse.getCreationdate()).isEqualTo(UPDATED_CREATIONDATE);
        assertThat(testListeCourse.getModificationdate()).isEqualTo(UPDATED_MODIFICATIONDATE);
        assertThat(testListeCourse.getTotalItems()).isEqualTo(UPDATED_TOTAL_ITEMS);
        assertThat(testListeCourse.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingListeCourse() throws Exception {
        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();
        listeCourse.setId(count.incrementAndGet());

        // Create the ListeCourse
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListeCourseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, listeCourseDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchListeCourse() throws Exception {
        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();
        listeCourse.setId(count.incrementAndGet());

        // Create the ListeCourse
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListeCourseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamListeCourse() throws Exception {
        int databaseSizeBeforeUpdate = listeCourseRepository.findAll().size();
        listeCourse.setId(count.incrementAndGet());

        // Create the ListeCourse
        ListeCourseDTO listeCourseDTO = listeCourseMapper.toDto(listeCourse);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListeCourseMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(listeCourseDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ListeCourse in the database
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteListeCourse() throws Exception {
        // Initialize the database
        listeCourseRepository.saveAndFlush(listeCourse);

        int databaseSizeBeforeDelete = listeCourseRepository.findAll().size();

        // Delete the listeCourse
        restListeCourseMockMvc
            .perform(delete(ENTITY_API_URL_ID, listeCourse.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ListeCourse> listeCourseList = listeCourseRepository.findAll();
        assertThat(listeCourseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
