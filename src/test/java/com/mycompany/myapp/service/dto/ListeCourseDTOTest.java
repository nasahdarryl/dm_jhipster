package com.mycompany.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ListeCourseDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListeCourseDTO.class);
        ListeCourseDTO listeCourseDTO1 = new ListeCourseDTO();
        listeCourseDTO1.setId(1L);
        ListeCourseDTO listeCourseDTO2 = new ListeCourseDTO();
        assertThat(listeCourseDTO1).isNotEqualTo(listeCourseDTO2);
        listeCourseDTO2.setId(listeCourseDTO1.getId());
        assertThat(listeCourseDTO1).isEqualTo(listeCourseDTO2);
        listeCourseDTO2.setId(2L);
        assertThat(listeCourseDTO1).isNotEqualTo(listeCourseDTO2);
        listeCourseDTO1.setId(null);
        assertThat(listeCourseDTO1).isNotEqualTo(listeCourseDTO2);
    }
}
