import { ISocietaire } from 'app/shared/model/societaire.model';

export interface ICooperative {
  id?: number;
  name?: string;
  location?: string;
  socnames?: ISocietaire[];
}

export const defaultValue: Readonly<ICooperative> = {};
