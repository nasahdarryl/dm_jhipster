import { ISocietaire } from 'app/shared/model/societaire.model';
import { IClient } from 'app/shared/model/client.model';
import { IArticle } from 'app/shared/model/article.model';

export interface ICommercant {
  id?: number;
  firstname?: string;
  lastName?: string | null;
  age?: number;
  address?: string | null;
  socfirstname?: ISocietaire;
  clients?: IClient[] | null;
  articles?: IArticle[] | null;
}

export const defaultValue: Readonly<ICommercant> = {};
