import dayjs from 'dayjs';
import { IClient } from 'app/shared/model/client.model';
import { IArticle } from 'app/shared/model/article.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface IListeCourse {
  id?: number;
  name?: string;
  creationdate?: string;
  modificationdate?: string;
  totalItems?: number;
  status?: Status;
  listname?: IClient;
  articles?: IArticle[] | null;
}

export const defaultValue: Readonly<IListeCourse> = {};
