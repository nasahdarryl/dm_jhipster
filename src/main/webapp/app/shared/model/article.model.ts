import { IListeCourse } from 'app/shared/model/liste-course.model';
import { ICommercant } from 'app/shared/model/commercant.model';

export interface IArticle {
  id?: number;
  name?: string;
  price?: number;
  listeCourses?: IListeCourse[] | null;
  commercants?: ICommercant[] | null;
}

export const defaultValue: Readonly<IArticle> = {};
