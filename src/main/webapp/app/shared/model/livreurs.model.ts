import { IClient } from 'app/shared/model/client.model';
import { ISocietaire } from 'app/shared/model/societaire.model';

export interface ILivreurs {
  id?: number;
  firstname?: string;
  lastName?: string | null;
  vehicle?: string;
  age?: number;
  clientfirstname?: IClient;
  socfirstname?: ISocietaire;
}

export const defaultValue: Readonly<ILivreurs> = {};
