import { ICooperative } from 'app/shared/model/cooperative.model';

export interface ISocietaire {
  id?: number;
  firstname?: string;
  lastName?: string | null;
  nomSociete?: string;
  role?: string;
  age?: number;
  coopname?: ICooperative;
}

export const defaultValue: Readonly<ISocietaire> = {};
