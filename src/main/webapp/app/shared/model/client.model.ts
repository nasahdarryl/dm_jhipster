import { ISocietaire } from 'app/shared/model/societaire.model';
import { IListeCourse } from 'app/shared/model/liste-course.model';
import { ICommercant } from 'app/shared/model/commercant.model';

export interface IClient {
  id?: number;
  firstName?: string;
  lastName?: string | null;
  age?: number;
  address?: string | null;
  socfirstname?: ISocietaire;
  listeCourses?: IListeCourse[] | null;
  commercants?: ICommercant[] | null;
}

export const defaultValue: Readonly<IClient> = {};
