import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IClient } from 'app/shared/model/client.model';
import { getEntities as getClients } from 'app/entities/client/client.reducer';
import { ISocietaire } from 'app/shared/model/societaire.model';
import { getEntities as getSocietaires } from 'app/entities/societaire/societaire.reducer';
import { ILivreurs } from 'app/shared/model/livreurs.model';
import { getEntity, updateEntity, createEntity, reset } from './livreurs.reducer';

export const LivreursUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const clients = useAppSelector(state => state.client.entities);
  const societaires = useAppSelector(state => state.societaire.entities);
  const livreursEntity = useAppSelector(state => state.livreurs.entity);
  const loading = useAppSelector(state => state.livreurs.loading);
  const updating = useAppSelector(state => state.livreurs.updating);
  const updateSuccess = useAppSelector(state => state.livreurs.updateSuccess);

  const handleClose = () => {
    navigate('/livreurs');
  };

  useEffect(() => {
    if (!isNew) {
      dispatch(getEntity(id));
    }

    dispatch(getClients({}));
    dispatch(getSocietaires({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...livreursEntity,
      ...values,
      clientfirstname: clients.find(it => it.id.toString() === values.clientfirstname.toString()),
      socfirstname: societaires.find(it => it.id.toString() === values.socfirstname.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...livreursEntity,
          clientfirstname: livreursEntity?.clientfirstname?.id,
          socfirstname: livreursEntity?.socfirstname?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="coopcycleApp.livreurs.home.createOrEditLabel" data-cy="LivreursCreateUpdateHeading">
            <Translate contentKey="coopcycleApp.livreurs.home.createOrEditLabel">Create or edit a Livreurs</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="livreurs-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('coopcycleApp.livreurs.firstname')}
                id="livreurs-firstname"
                name="firstname"
                data-cy="firstname"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  pattern: { value: /^[A-Z][a-z]+\d$/, message: translate('entity.validation.pattern', { pattern: '^[A-Z][a-z]+\\d$' }) },
                }}
              />
              <ValidatedField
                label={translate('coopcycleApp.livreurs.lastName')}
                id="livreurs-lastName"
                name="lastName"
                data-cy="lastName"
                type="text"
                validate={{
                  pattern: { value: /^[A-Z][a-z]+\d$/, message: translate('entity.validation.pattern', { pattern: '^[A-Z][a-z]+\\d$' }) },
                }}
              />
              <ValidatedField
                label={translate('coopcycleApp.livreurs.vehicle')}
                id="livreurs-vehicle"
                name="vehicle"
                data-cy="vehicle"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('coopcycleApp.livreurs.age')}
                id="livreurs-age"
                name="age"
                data-cy="age"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  min: { value: 18, message: translate('entity.validation.min', { min: 18 }) },
                  max: { value: 100, message: translate('entity.validation.max', { max: 100 }) },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                id="livreurs-clientfirstname"
                name="clientfirstname"
                data-cy="clientfirstname"
                label={translate('coopcycleApp.livreurs.clientfirstname')}
                type="select"
                required
              >
                <option value="" key="0" />
                {clients
                  ? clients.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <FormText>
                <Translate contentKey="entity.validation.required">This field is required.</Translate>
              </FormText>
              <ValidatedField
                id="livreurs-socfirstname"
                name="socfirstname"
                data-cy="socfirstname"
                label={translate('coopcycleApp.livreurs.socfirstname')}
                type="select"
                required
              >
                <option value="" key="0" />
                {societaires
                  ? societaires.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <FormText>
                <Translate contentKey="entity.validation.required">This field is required.</Translate>
              </FormText>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/livreurs" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default LivreursUpdate;
