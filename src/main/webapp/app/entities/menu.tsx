import React from 'react';
import { Translate } from 'react-jhipster';

import MenuItem from 'app/shared/layout/menus/menu-item';

const EntitiesMenu = () => {
  return (
    <>
      {/* prettier-ignore */}
      <MenuItem icon="asterisk" to="/client">
        <Translate contentKey="global.menu.entities.client" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/commercant">
        <Translate contentKey="global.menu.entities.commercant" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/livreurs">
        <Translate contentKey="global.menu.entities.livreurs" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/societaire">
        <Translate contentKey="global.menu.entities.societaire" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/article">
        <Translate contentKey="global.menu.entities.article" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/liste-course">
        <Translate contentKey="global.menu.entities.listeCourse" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/cooperative">
        <Translate contentKey="global.menu.entities.cooperative" />
      </MenuItem>
      {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
    </>
  );
};

export default EntitiesMenu;
