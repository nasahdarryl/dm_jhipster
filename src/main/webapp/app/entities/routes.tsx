import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Client from './client';
import Commercant from './commercant';
import Livreurs from './livreurs';
import Societaire from './societaire';
import Article from './article';
import ListeCourse from './liste-course';
import Cooperative from './cooperative';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default () => {
  return (
    <div>
      <ErrorBoundaryRoutes>
        {/* prettier-ignore */}
        <Route path="client/*" element={<Client />} />
        <Route path="commercant/*" element={<Commercant />} />
        <Route path="livreurs/*" element={<Livreurs />} />
        <Route path="societaire/*" element={<Societaire />} />
        <Route path="article/*" element={<Article />} />
        <Route path="liste-course/*" element={<ListeCourse />} />
        <Route path="cooperative/*" element={<Cooperative />} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </ErrorBoundaryRoutes>
    </div>
  );
};
