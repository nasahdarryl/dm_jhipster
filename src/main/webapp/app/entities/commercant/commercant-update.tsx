import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { ISocietaire } from 'app/shared/model/societaire.model';
import { getEntities as getSocietaires } from 'app/entities/societaire/societaire.reducer';
import { IClient } from 'app/shared/model/client.model';
import { getEntities as getClients } from 'app/entities/client/client.reducer';
import { IArticle } from 'app/shared/model/article.model';
import { getEntities as getArticles } from 'app/entities/article/article.reducer';
import { ICommercant } from 'app/shared/model/commercant.model';
import { getEntity, updateEntity, createEntity, reset } from './commercant.reducer';

export const CommercantUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const societaires = useAppSelector(state => state.societaire.entities);
  const clients = useAppSelector(state => state.client.entities);
  const articles = useAppSelector(state => state.article.entities);
  const commercantEntity = useAppSelector(state => state.commercant.entity);
  const loading = useAppSelector(state => state.commercant.loading);
  const updating = useAppSelector(state => state.commercant.updating);
  const updateSuccess = useAppSelector(state => state.commercant.updateSuccess);

  const handleClose = () => {
    navigate('/commercant');
  };

  useEffect(() => {
    if (!isNew) {
      dispatch(getEntity(id));
    }

    dispatch(getSocietaires({}));
    dispatch(getClients({}));
    dispatch(getArticles({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...commercantEntity,
      ...values,
      clients: mapIdList(values.clients),
      socfirstname: societaires.find(it => it.id.toString() === values.socfirstname.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...commercantEntity,
          socfirstname: commercantEntity?.socfirstname?.id,
          clients: commercantEntity?.clients?.map(e => e.id.toString()),
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="coopcycleApp.commercant.home.createOrEditLabel" data-cy="CommercantCreateUpdateHeading">
            <Translate contentKey="coopcycleApp.commercant.home.createOrEditLabel">Create or edit a Commercant</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="commercant-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('coopcycleApp.commercant.firstname')}
                id="commercant-firstname"
                name="firstname"
                data-cy="firstname"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  pattern: { value: /^[A-Z][a-z]+\d$/, message: translate('entity.validation.pattern', { pattern: '^[A-Z][a-z]+\\d$' }) },
                }}
              />
              <ValidatedField
                label={translate('coopcycleApp.commercant.lastName')}
                id="commercant-lastName"
                name="lastName"
                data-cy="lastName"
                type="text"
                validate={{
                  pattern: { value: /^[A-Z][a-z]+\d$/, message: translate('entity.validation.pattern', { pattern: '^[A-Z][a-z]+\\d$' }) },
                }}
              />
              <ValidatedField
                label={translate('coopcycleApp.commercant.age')}
                id="commercant-age"
                name="age"
                data-cy="age"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  min: { value: 18, message: translate('entity.validation.min', { min: 18 }) },
                  max: { value: 100, message: translate('entity.validation.max', { max: 100 }) },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                label={translate('coopcycleApp.commercant.address')}
                id="commercant-address"
                name="address"
                data-cy="address"
                type="text"
              />
              <ValidatedField
                id="commercant-socfirstname"
                name="socfirstname"
                data-cy="socfirstname"
                label={translate('coopcycleApp.commercant.socfirstname')}
                type="select"
                required
              >
                <option value="" key="0" />
                {societaires
                  ? societaires.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <FormText>
                <Translate contentKey="entity.validation.required">This field is required.</Translate>
              </FormText>
              <ValidatedField
                label={translate('coopcycleApp.commercant.client')}
                id="commercant-client"
                data-cy="client"
                type="select"
                multiple
                name="clients"
              >
                <option value="" key="0" />
                {clients
                  ? clients.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/commercant" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CommercantUpdate;
