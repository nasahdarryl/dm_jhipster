import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './commercant.reducer';

export const CommercantDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const commercantEntity = useAppSelector(state => state.commercant.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="commercantDetailsHeading">
          <Translate contentKey="coopcycleApp.commercant.detail.title">Commercant</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{commercantEntity.id}</dd>
          <dt>
            <span id="firstname">
              <Translate contentKey="coopcycleApp.commercant.firstname">Firstname</Translate>
            </span>
          </dt>
          <dd>{commercantEntity.firstname}</dd>
          <dt>
            <span id="lastName">
              <Translate contentKey="coopcycleApp.commercant.lastName">Last Name</Translate>
            </span>
          </dt>
          <dd>{commercantEntity.lastName}</dd>
          <dt>
            <span id="age">
              <Translate contentKey="coopcycleApp.commercant.age">Age</Translate>
            </span>
          </dt>
          <dd>{commercantEntity.age}</dd>
          <dt>
            <span id="address">
              <Translate contentKey="coopcycleApp.commercant.address">Address</Translate>
            </span>
          </dt>
          <dd>{commercantEntity.address}</dd>
          <dt>
            <Translate contentKey="coopcycleApp.commercant.socfirstname">Socfirstname</Translate>
          </dt>
          <dd>{commercantEntity.socfirstname ? commercantEntity.socfirstname.id : ''}</dd>
          <dt>
            <Translate contentKey="coopcycleApp.commercant.client">Client</Translate>
          </dt>
          <dd>
            {commercantEntity.clients
              ? commercantEntity.clients.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {commercantEntity.clients && i === commercantEntity.clients.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/commercant" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/commercant/${commercantEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default CommercantDetail;
