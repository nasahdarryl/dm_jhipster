import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import ListeCourse from './liste-course';
import ListeCourseDetail from './liste-course-detail';
import ListeCourseUpdate from './liste-course-update';
import ListeCourseDeleteDialog from './liste-course-delete-dialog';

const ListeCourseRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<ListeCourse />} />
    <Route path="new" element={<ListeCourseUpdate />} />
    <Route path=":id">
      <Route index element={<ListeCourseDetail />} />
      <Route path="edit" element={<ListeCourseUpdate />} />
      <Route path="delete" element={<ListeCourseDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default ListeCourseRoutes;
