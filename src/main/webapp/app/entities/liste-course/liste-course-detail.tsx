import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './liste-course.reducer';

export const ListeCourseDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const listeCourseEntity = useAppSelector(state => state.listeCourse.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="listeCourseDetailsHeading">
          <Translate contentKey="coopcycleApp.listeCourse.detail.title">ListeCourse</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{listeCourseEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="coopcycleApp.listeCourse.name">Name</Translate>
            </span>
          </dt>
          <dd>{listeCourseEntity.name}</dd>
          <dt>
            <span id="creationdate">
              <Translate contentKey="coopcycleApp.listeCourse.creationdate">Creationdate</Translate>
            </span>
          </dt>
          <dd>
            {listeCourseEntity.creationdate ? (
              <TextFormat value={listeCourseEntity.creationdate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="modificationdate">
              <Translate contentKey="coopcycleApp.listeCourse.modificationdate">Modificationdate</Translate>
            </span>
          </dt>
          <dd>
            {listeCourseEntity.modificationdate ? (
              <TextFormat value={listeCourseEntity.modificationdate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="totalItems">
              <Translate contentKey="coopcycleApp.listeCourse.totalItems">Total Items</Translate>
            </span>
          </dt>
          <dd>{listeCourseEntity.totalItems}</dd>
          <dt>
            <span id="status">
              <Translate contentKey="coopcycleApp.listeCourse.status">Status</Translate>
            </span>
          </dt>
          <dd>{listeCourseEntity.status}</dd>
          <dt>
            <Translate contentKey="coopcycleApp.listeCourse.listname">Listname</Translate>
          </dt>
          <dd>{listeCourseEntity.listname ? listeCourseEntity.listname.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/liste-course" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/liste-course/${listeCourseEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default ListeCourseDetail;
