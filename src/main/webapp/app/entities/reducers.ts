import client from 'app/entities/client/client.reducer';
import commercant from 'app/entities/commercant/commercant.reducer';
import livreurs from 'app/entities/livreurs/livreurs.reducer';
import societaire from 'app/entities/societaire/societaire.reducer';
import article from 'app/entities/article/article.reducer';
import listeCourse from 'app/entities/liste-course/liste-course.reducer';
import cooperative from 'app/entities/cooperative/cooperative.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  client,
  commercant,
  livreurs,
  societaire,
  article,
  listeCourse,
  cooperative,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
