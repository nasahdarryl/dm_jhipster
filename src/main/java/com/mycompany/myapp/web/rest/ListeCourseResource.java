package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.ListeCourseRepository;
import com.mycompany.myapp.service.ListeCourseService;
import com.mycompany.myapp.service.dto.ListeCourseDTO;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ListeCourse}.
 */
@RestController
@RequestMapping("/api")
public class ListeCourseResource {

    private final Logger log = LoggerFactory.getLogger(ListeCourseResource.class);

    private static final String ENTITY_NAME = "listeCourse";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ListeCourseService listeCourseService;

    private final ListeCourseRepository listeCourseRepository;

    public ListeCourseResource(ListeCourseService listeCourseService, ListeCourseRepository listeCourseRepository) {
        this.listeCourseService = listeCourseService;
        this.listeCourseRepository = listeCourseRepository;
    }

    /**
     * {@code POST  /liste-courses} : Create a new listeCourse.
     *
     * @param listeCourseDTO the listeCourseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new listeCourseDTO, or with status {@code 400 (Bad Request)} if the listeCourse has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/liste-courses")
    public ResponseEntity<ListeCourseDTO> createListeCourse(@Valid @RequestBody ListeCourseDTO listeCourseDTO) throws URISyntaxException {
        log.debug("REST request to save ListeCourse : {}", listeCourseDTO);
        if (listeCourseDTO.getId() != null) {
            throw new BadRequestAlertException("A new listeCourse cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ListeCourseDTO result = listeCourseService.save(listeCourseDTO);
        return ResponseEntity
            .created(new URI("/api/liste-courses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /liste-courses/:id} : Updates an existing listeCourse.
     *
     * @param id the id of the listeCourseDTO to save.
     * @param listeCourseDTO the listeCourseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listeCourseDTO,
     * or with status {@code 400 (Bad Request)} if the listeCourseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the listeCourseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/liste-courses/{id}")
    public ResponseEntity<ListeCourseDTO> updateListeCourse(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ListeCourseDTO listeCourseDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ListeCourse : {}, {}", id, listeCourseDTO);
        if (listeCourseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, listeCourseDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!listeCourseRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ListeCourseDTO result = listeCourseService.update(listeCourseDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listeCourseDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /liste-courses/:id} : Partial updates given fields of an existing listeCourse, field will ignore if it is null
     *
     * @param id the id of the listeCourseDTO to save.
     * @param listeCourseDTO the listeCourseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listeCourseDTO,
     * or with status {@code 400 (Bad Request)} if the listeCourseDTO is not valid,
     * or with status {@code 404 (Not Found)} if the listeCourseDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the listeCourseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/liste-courses/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ListeCourseDTO> partialUpdateListeCourse(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ListeCourseDTO listeCourseDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ListeCourse partially : {}, {}", id, listeCourseDTO);
        if (listeCourseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, listeCourseDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!listeCourseRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ListeCourseDTO> result = listeCourseService.partialUpdate(listeCourseDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listeCourseDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /liste-courses} : get all the listeCourses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of listeCourses in body.
     */
    @GetMapping("/liste-courses")
    public ResponseEntity<List<ListeCourseDTO>> getAllListeCourses(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ListeCourses");
        Page<ListeCourseDTO> page = listeCourseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /liste-courses/:id} : get the "id" listeCourse.
     *
     * @param id the id of the listeCourseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the listeCourseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/liste-courses/{id}")
    public ResponseEntity<ListeCourseDTO> getListeCourse(@PathVariable Long id) {
        log.debug("REST request to get ListeCourse : {}", id);
        Optional<ListeCourseDTO> listeCourseDTO = listeCourseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(listeCourseDTO);
    }

    /**
     * {@code DELETE  /liste-courses/:id} : delete the "id" listeCourse.
     *
     * @param id the id of the listeCourseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/liste-courses/{id}")
    public ResponseEntity<Void> deleteListeCourse(@PathVariable Long id) {
        log.debug("REST request to delete ListeCourse : {}", id);
        listeCourseService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
