package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.enumeration.Status;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ListeCourse.
 */
@Entity
@Table(name = "liste_course")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ListeCourse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "creationdate", nullable = false)
    private LocalDate creationdate;

    @NotNull
    @Column(name = "modificationdate", nullable = false)
    private LocalDate modificationdate;

    @NotNull
    @Column(name = "total_items", nullable = false)
    private Long totalItems;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "socfirstname", "listeCourses", "commercants" }, allowSetters = true)
    private Client listname;

    @ManyToMany(mappedBy = "listeCourses")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "listeCourses", "commercants" }, allowSetters = true)
    private Set<Article> articles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ListeCourse id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public ListeCourse name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreationdate() {
        return this.creationdate;
    }

    public ListeCourse creationdate(LocalDate creationdate) {
        this.setCreationdate(creationdate);
        return this;
    }

    public void setCreationdate(LocalDate creationdate) {
        this.creationdate = creationdate;
    }

    public LocalDate getModificationdate() {
        return this.modificationdate;
    }

    public ListeCourse modificationdate(LocalDate modificationdate) {
        this.setModificationdate(modificationdate);
        return this;
    }

    public void setModificationdate(LocalDate modificationdate) {
        this.modificationdate = modificationdate;
    }

    public Long getTotalItems() {
        return this.totalItems;
    }

    public ListeCourse totalItems(Long totalItems) {
        this.setTotalItems(totalItems);
        return this;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public Status getStatus() {
        return this.status;
    }

    public ListeCourse status(Status status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Client getListname() {
        return this.listname;
    }

    public void setListname(Client client) {
        this.listname = client;
    }

    public ListeCourse listname(Client client) {
        this.setListname(client);
        return this;
    }

    public Set<Article> getArticles() {
        return this.articles;
    }

    public void setArticles(Set<Article> articles) {
        if (this.articles != null) {
            this.articles.forEach(i -> i.removeListeCourse(this));
        }
        if (articles != null) {
            articles.forEach(i -> i.addListeCourse(this));
        }
        this.articles = articles;
    }

    public ListeCourse articles(Set<Article> articles) {
        this.setArticles(articles);
        return this;
    }

    public ListeCourse addArticle(Article article) {
        this.articles.add(article);
        article.getListeCourses().add(this);
        return this;
    }

    public ListeCourse removeArticle(Article article) {
        this.articles.remove(article);
        article.getListeCourses().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ListeCourse)) {
            return false;
        }
        return id != null && id.equals(((ListeCourse) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ListeCourse{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", creationdate='" + getCreationdate() + "'" +
            ", modificationdate='" + getModificationdate() + "'" +
            ", totalItems=" + getTotalItems() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
