package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Societaire.
 */
@Entity
@Table(name = "societaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Societaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "nom_societe", nullable = false)
    private String nomSociete;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "role", nullable = false)
    private String role;

    @NotNull
    @Min(value = 18L)
    @Max(value = 100L)
    @Column(name = "age", nullable = false)
    private Long age;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "socnames" }, allowSetters = true)
    private Cooperative coopname;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Societaire id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public Societaire firstname(String firstname) {
        this.setFirstname(firstname);
        return this;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Societaire lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNomSociete() {
        return this.nomSociete;
    }

    public Societaire nomSociete(String nomSociete) {
        this.setNomSociete(nomSociete);
        return this;
    }

    public void setNomSociete(String nomSociete) {
        this.nomSociete = nomSociete;
    }

    public String getRole() {
        return this.role;
    }

    public Societaire role(String role) {
        this.setRole(role);
        return this;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getAge() {
        return this.age;
    }

    public Societaire age(Long age) {
        this.setAge(age);
        return this;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Cooperative getCoopname() {
        return this.coopname;
    }

    public void setCoopname(Cooperative cooperative) {
        this.coopname = cooperative;
    }

    public Societaire coopname(Cooperative cooperative) {
        this.setCoopname(cooperative);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Societaire)) {
            return false;
        }
        return id != null && id.equals(((Societaire) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Societaire{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", nomSociete='" + getNomSociete() + "'" +
            ", role='" + getRole() + "'" +
            ", age=" + getAge() +
            "}";
    }
}
