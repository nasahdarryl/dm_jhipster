package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Commercant.
 */
@Entity
@Table(name = "commercant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Commercant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Min(value = 18L)
    @Max(value = 100L)
    @Column(name = "age", nullable = false)
    private Long age;

    @Column(name = "address")
    private String address;

    @JsonIgnoreProperties(value = { "coopname" }, allowSetters = true)
    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Societaire socfirstname;

    @ManyToMany
    @JoinTable(
        name = "rel_commercant__client",
        joinColumns = @JoinColumn(name = "commercant_id"),
        inverseJoinColumns = @JoinColumn(name = "client_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "socfirstname", "listeCourses", "commercants" }, allowSetters = true)
    private Set<Client> clients = new HashSet<>();

    @ManyToMany(mappedBy = "commercants")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "listeCourses", "commercants" }, allowSetters = true)
    private Set<Article> articles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Commercant id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public Commercant firstname(String firstname) {
        this.setFirstname(firstname);
        return this;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Commercant lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getAge() {
        return this.age;
    }

    public Commercant age(Long age) {
        this.setAge(age);
        return this;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getAddress() {
        return this.address;
    }

    public Commercant address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Societaire getSocfirstname() {
        return this.socfirstname;
    }

    public void setSocfirstname(Societaire societaire) {
        this.socfirstname = societaire;
    }

    public Commercant socfirstname(Societaire societaire) {
        this.setSocfirstname(societaire);
        return this;
    }

    public Set<Client> getClients() {
        return this.clients;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

    public Commercant clients(Set<Client> clients) {
        this.setClients(clients);
        return this;
    }

    public Commercant addClient(Client client) {
        this.clients.add(client);
        client.getCommercants().add(this);
        return this;
    }

    public Commercant removeClient(Client client) {
        this.clients.remove(client);
        client.getCommercants().remove(this);
        return this;
    }

    public Set<Article> getArticles() {
        return this.articles;
    }

    public void setArticles(Set<Article> articles) {
        if (this.articles != null) {
            this.articles.forEach(i -> i.removeCommercant(this));
        }
        if (articles != null) {
            articles.forEach(i -> i.addCommercant(this));
        }
        this.articles = articles;
    }

    public Commercant articles(Set<Article> articles) {
        this.setArticles(articles);
        return this;
    }

    public Commercant addArticle(Article article) {
        this.articles.add(article);
        article.getCommercants().add(this);
        return this;
    }

    public Commercant removeArticle(Article article) {
        this.articles.remove(article);
        article.getCommercants().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Commercant)) {
            return false;
        }
        return id != null && id.equals(((Commercant) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Commercant{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", age=" + getAge() +
            ", address='" + getAddress() + "'" +
            "}";
    }
}
