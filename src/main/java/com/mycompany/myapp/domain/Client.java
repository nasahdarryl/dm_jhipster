package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Min(value = 18L)
    @Max(value = 100L)
    @Column(name = "age", nullable = false)
    private Long age;

    @Column(name = "address")
    private String address;

    @JsonIgnoreProperties(value = { "coopname" }, allowSetters = true)
    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Societaire socfirstname;

    @OneToMany(mappedBy = "listname")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "listname", "articles" }, allowSetters = true)
    private Set<ListeCourse> listeCourses = new HashSet<>();

    @ManyToMany(mappedBy = "clients")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "socfirstname", "clients", "articles" }, allowSetters = true)
    private Set<Commercant> commercants = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Client id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Client firstName(String firstName) {
        this.setFirstName(firstName);
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Client lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getAge() {
        return this.age;
    }

    public Client age(Long age) {
        this.setAge(age);
        return this;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getAddress() {
        return this.address;
    }

    public Client address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Societaire getSocfirstname() {
        return this.socfirstname;
    }

    public void setSocfirstname(Societaire societaire) {
        this.socfirstname = societaire;
    }

    public Client socfirstname(Societaire societaire) {
        this.setSocfirstname(societaire);
        return this;
    }

    public Set<ListeCourse> getListeCourses() {
        return this.listeCourses;
    }

    public void setListeCourses(Set<ListeCourse> listeCourses) {
        if (this.listeCourses != null) {
            this.listeCourses.forEach(i -> i.setListname(null));
        }
        if (listeCourses != null) {
            listeCourses.forEach(i -> i.setListname(this));
        }
        this.listeCourses = listeCourses;
    }

    public Client listeCourses(Set<ListeCourse> listeCourses) {
        this.setListeCourses(listeCourses);
        return this;
    }

    public Client addListeCourse(ListeCourse listeCourse) {
        this.listeCourses.add(listeCourse);
        listeCourse.setListname(this);
        return this;
    }

    public Client removeListeCourse(ListeCourse listeCourse) {
        this.listeCourses.remove(listeCourse);
        listeCourse.setListname(null);
        return this;
    }

    public Set<Commercant> getCommercants() {
        return this.commercants;
    }

    public void setCommercants(Set<Commercant> commercants) {
        if (this.commercants != null) {
            this.commercants.forEach(i -> i.removeClient(this));
        }
        if (commercants != null) {
            commercants.forEach(i -> i.addClient(this));
        }
        this.commercants = commercants;
    }

    public Client commercants(Set<Commercant> commercants) {
        this.setCommercants(commercants);
        return this;
    }

    public Client addCommercant(Commercant commercant) {
        this.commercants.add(commercant);
        commercant.getClients().add(this);
        return this;
    }

    public Client removeCommercant(Commercant commercant) {
        this.commercants.remove(commercant);
        commercant.getClients().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", age=" + getAge() +
            ", address='" + getAddress() + "'" +
            "}";
    }
}
