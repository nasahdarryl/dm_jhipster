package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Livreurs.
 */
@Entity
@Table(name = "livreurs")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Livreurs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Column(name = "vehicle", nullable = false)
    private String vehicle;

    @NotNull
    @Min(value = 18L)
    @Max(value = 100L)
    @Column(name = "age", nullable = false)
    private Long age;

    @JsonIgnoreProperties(value = { "socfirstname", "listeCourses", "commercants" }, allowSetters = true)
    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Client clientfirstname;

    @JsonIgnoreProperties(value = { "coopname" }, allowSetters = true)
    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Societaire socfirstname;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Livreurs id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public Livreurs firstname(String firstname) {
        this.setFirstname(firstname);
        return this;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Livreurs lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getVehicle() {
        return this.vehicle;
    }

    public Livreurs vehicle(String vehicle) {
        this.setVehicle(vehicle);
        return this;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Long getAge() {
        return this.age;
    }

    public Livreurs age(Long age) {
        this.setAge(age);
        return this;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Client getClientfirstname() {
        return this.clientfirstname;
    }

    public void setClientfirstname(Client client) {
        this.clientfirstname = client;
    }

    public Livreurs clientfirstname(Client client) {
        this.setClientfirstname(client);
        return this;
    }

    public Societaire getSocfirstname() {
        return this.socfirstname;
    }

    public void setSocfirstname(Societaire societaire) {
        this.socfirstname = societaire;
    }

    public Livreurs socfirstname(Societaire societaire) {
        this.setSocfirstname(societaire);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Livreurs)) {
            return false;
        }
        return id != null && id.equals(((Livreurs) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Livreurs{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", vehicle='" + getVehicle() + "'" +
            ", age=" + getAge() +
            "}";
    }
}
