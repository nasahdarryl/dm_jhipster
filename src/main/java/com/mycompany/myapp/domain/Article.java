package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Article.
 */
@Entity
@Table(name = "article")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "price", nullable = false)
    private Long price;

    @ManyToMany
    @JoinTable(
        name = "rel_article__liste_course",
        joinColumns = @JoinColumn(name = "article_id"),
        inverseJoinColumns = @JoinColumn(name = "liste_course_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "listname", "articles" }, allowSetters = true)
    private Set<ListeCourse> listeCourses = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_article__commercant",
        joinColumns = @JoinColumn(name = "article_id"),
        inverseJoinColumns = @JoinColumn(name = "commercant_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "socfirstname", "clients", "articles" }, allowSetters = true)
    private Set<Commercant> commercants = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Article id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Article name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return this.price;
    }

    public Article price(Long price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Set<ListeCourse> getListeCourses() {
        return this.listeCourses;
    }

    public void setListeCourses(Set<ListeCourse> listeCourses) {
        this.listeCourses = listeCourses;
    }

    public Article listeCourses(Set<ListeCourse> listeCourses) {
        this.setListeCourses(listeCourses);
        return this;
    }

    public Article addListeCourse(ListeCourse listeCourse) {
        this.listeCourses.add(listeCourse);
        listeCourse.getArticles().add(this);
        return this;
    }

    public Article removeListeCourse(ListeCourse listeCourse) {
        this.listeCourses.remove(listeCourse);
        listeCourse.getArticles().remove(this);
        return this;
    }

    public Set<Commercant> getCommercants() {
        return this.commercants;
    }

    public void setCommercants(Set<Commercant> commercants) {
        this.commercants = commercants;
    }

    public Article commercants(Set<Commercant> commercants) {
        this.setCommercants(commercants);
        return this;
    }

    public Article addCommercant(Commercant commercant) {
        this.commercants.add(commercant);
        commercant.getArticles().add(this);
        return this;
    }

    public Article removeCommercant(Commercant commercant) {
        this.commercants.remove(commercant);
        commercant.getArticles().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Article)) {
            return false;
        }
        return id != null && id.equals(((Article) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Article{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
