package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ListeCourse;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ListeCourse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListeCourseRepository extends JpaRepository<ListeCourse, Long> {}
