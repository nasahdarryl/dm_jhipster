package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Commercant;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface CommercantRepositoryWithBagRelationships {
    Optional<Commercant> fetchBagRelationships(Optional<Commercant> commercant);

    List<Commercant> fetchBagRelationships(List<Commercant> commercants);

    Page<Commercant> fetchBagRelationships(Page<Commercant> commercants);
}
