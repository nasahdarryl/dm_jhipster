package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Commercant;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class CommercantRepositoryWithBagRelationshipsImpl implements CommercantRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Commercant> fetchBagRelationships(Optional<Commercant> commercant) {
        return commercant.map(this::fetchClients);
    }

    @Override
    public Page<Commercant> fetchBagRelationships(Page<Commercant> commercants) {
        return new PageImpl<>(fetchBagRelationships(commercants.getContent()), commercants.getPageable(), commercants.getTotalElements());
    }

    @Override
    public List<Commercant> fetchBagRelationships(List<Commercant> commercants) {
        return Optional.of(commercants).map(this::fetchClients).orElse(Collections.emptyList());
    }

    Commercant fetchClients(Commercant result) {
        return entityManager
            .createQuery(
                "select commercant from Commercant commercant left join fetch commercant.clients where commercant is :commercant",
                Commercant.class
            )
            .setParameter("commercant", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Commercant> fetchClients(List<Commercant> commercants) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, commercants.size()).forEach(index -> order.put(commercants.get(index).getId(), index));
        List<Commercant> result = entityManager
            .createQuery(
                "select distinct commercant from Commercant commercant left join fetch commercant.clients where commercant in :commercants",
                Commercant.class
            )
            .setParameter("commercants", commercants)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
