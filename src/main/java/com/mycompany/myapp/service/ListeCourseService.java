package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ListeCourse;
import com.mycompany.myapp.repository.ListeCourseRepository;
import com.mycompany.myapp.service.dto.ListeCourseDTO;
import com.mycompany.myapp.service.mapper.ListeCourseMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ListeCourse}.
 */
@Service
@Transactional
public class ListeCourseService {

    private final Logger log = LoggerFactory.getLogger(ListeCourseService.class);

    private final ListeCourseRepository listeCourseRepository;

    private final ListeCourseMapper listeCourseMapper;

    public ListeCourseService(ListeCourseRepository listeCourseRepository, ListeCourseMapper listeCourseMapper) {
        this.listeCourseRepository = listeCourseRepository;
        this.listeCourseMapper = listeCourseMapper;
    }

    /**
     * Save a listeCourse.
     *
     * @param listeCourseDTO the entity to save.
     * @return the persisted entity.
     */
    public ListeCourseDTO save(ListeCourseDTO listeCourseDTO) {
        log.debug("Request to save ListeCourse : {}", listeCourseDTO);
        ListeCourse listeCourse = listeCourseMapper.toEntity(listeCourseDTO);
        listeCourse = listeCourseRepository.save(listeCourse);
        return listeCourseMapper.toDto(listeCourse);
    }

    /**
     * Update a listeCourse.
     *
     * @param listeCourseDTO the entity to save.
     * @return the persisted entity.
     */
    public ListeCourseDTO update(ListeCourseDTO listeCourseDTO) {
        log.debug("Request to update ListeCourse : {}", listeCourseDTO);
        ListeCourse listeCourse = listeCourseMapper.toEntity(listeCourseDTO);
        listeCourse = listeCourseRepository.save(listeCourse);
        return listeCourseMapper.toDto(listeCourse);
    }

    /**
     * Partially update a listeCourse.
     *
     * @param listeCourseDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ListeCourseDTO> partialUpdate(ListeCourseDTO listeCourseDTO) {
        log.debug("Request to partially update ListeCourse : {}", listeCourseDTO);

        return listeCourseRepository
            .findById(listeCourseDTO.getId())
            .map(existingListeCourse -> {
                listeCourseMapper.partialUpdate(existingListeCourse, listeCourseDTO);

                return existingListeCourse;
            })
            .map(listeCourseRepository::save)
            .map(listeCourseMapper::toDto);
    }

    /**
     * Get all the listeCourses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ListeCourseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ListeCourses");
        return listeCourseRepository.findAll(pageable).map(listeCourseMapper::toDto);
    }

    /**
     * Get one listeCourse by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ListeCourseDTO> findOne(Long id) {
        log.debug("Request to get ListeCourse : {}", id);
        return listeCourseRepository.findById(id).map(listeCourseMapper::toDto);
    }

    /**
     * Delete the listeCourse by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ListeCourse : {}", id);
        listeCourseRepository.deleteById(id);
    }
}
