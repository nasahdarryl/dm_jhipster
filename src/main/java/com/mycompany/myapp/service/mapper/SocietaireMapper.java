package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Cooperative;
import com.mycompany.myapp.domain.Societaire;
import com.mycompany.myapp.service.dto.CooperativeDTO;
import com.mycompany.myapp.service.dto.SocietaireDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Societaire} and its DTO {@link SocietaireDTO}.
 */
@Mapper(componentModel = "spring")
public interface SocietaireMapper extends EntityMapper<SocietaireDTO, Societaire> {
    @Mapping(target = "coopname", source = "coopname", qualifiedByName = "cooperativeId")
    SocietaireDTO toDto(Societaire s);

    @Named("cooperativeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CooperativeDTO toDtoCooperativeId(Cooperative cooperative);
}
