package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.Livreurs;
import com.mycompany.myapp.domain.Societaire;
import com.mycompany.myapp.service.dto.ClientDTO;
import com.mycompany.myapp.service.dto.LivreursDTO;
import com.mycompany.myapp.service.dto.SocietaireDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Livreurs} and its DTO {@link LivreursDTO}.
 */
@Mapper(componentModel = "spring")
public interface LivreursMapper extends EntityMapper<LivreursDTO, Livreurs> {
    @Mapping(target = "clientfirstname", source = "clientfirstname", qualifiedByName = "clientId")
    @Mapping(target = "socfirstname", source = "socfirstname", qualifiedByName = "societaireId")
    LivreursDTO toDto(Livreurs s);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);

    @Named("societaireId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    SocietaireDTO toDtoSocietaireId(Societaire societaire);
}
