package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Article;
import com.mycompany.myapp.domain.Commercant;
import com.mycompany.myapp.domain.ListeCourse;
import com.mycompany.myapp.service.dto.ArticleDTO;
import com.mycompany.myapp.service.dto.CommercantDTO;
import com.mycompany.myapp.service.dto.ListeCourseDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Article} and its DTO {@link ArticleDTO}.
 */
@Mapper(componentModel = "spring")
public interface ArticleMapper extends EntityMapper<ArticleDTO, Article> {
    @Mapping(target = "listeCourses", source = "listeCourses", qualifiedByName = "listeCourseIdSet")
    @Mapping(target = "commercants", source = "commercants", qualifiedByName = "commercantIdSet")
    ArticleDTO toDto(Article s);

    @Mapping(target = "removeListeCourse", ignore = true)
    @Mapping(target = "removeCommercant", ignore = true)
    Article toEntity(ArticleDTO articleDTO);

    @Named("listeCourseId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ListeCourseDTO toDtoListeCourseId(ListeCourse listeCourse);

    @Named("listeCourseIdSet")
    default Set<ListeCourseDTO> toDtoListeCourseIdSet(Set<ListeCourse> listeCourse) {
        return listeCourse.stream().map(this::toDtoListeCourseId).collect(Collectors.toSet());
    }

    @Named("commercantId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommercantDTO toDtoCommercantId(Commercant commercant);

    @Named("commercantIdSet")
    default Set<CommercantDTO> toDtoCommercantIdSet(Set<Commercant> commercant) {
        return commercant.stream().map(this::toDtoCommercantId).collect(Collectors.toSet());
    }
}
