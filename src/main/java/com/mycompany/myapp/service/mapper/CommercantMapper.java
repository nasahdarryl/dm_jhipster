package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.Commercant;
import com.mycompany.myapp.domain.Societaire;
import com.mycompany.myapp.service.dto.ClientDTO;
import com.mycompany.myapp.service.dto.CommercantDTO;
import com.mycompany.myapp.service.dto.SocietaireDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Commercant} and its DTO {@link CommercantDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommercantMapper extends EntityMapper<CommercantDTO, Commercant> {
    @Mapping(target = "socfirstname", source = "socfirstname", qualifiedByName = "societaireId")
    @Mapping(target = "clients", source = "clients", qualifiedByName = "clientIdSet")
    CommercantDTO toDto(Commercant s);

    @Mapping(target = "removeClient", ignore = true)
    Commercant toEntity(CommercantDTO commercantDTO);

    @Named("societaireId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    SocietaireDTO toDtoSocietaireId(Societaire societaire);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);

    @Named("clientIdSet")
    default Set<ClientDTO> toDtoClientIdSet(Set<Client> client) {
        return client.stream().map(this::toDtoClientId).collect(Collectors.toSet());
    }
}
