package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Client;
import com.mycompany.myapp.domain.ListeCourse;
import com.mycompany.myapp.service.dto.ClientDTO;
import com.mycompany.myapp.service.dto.ListeCourseDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ListeCourse} and its DTO {@link ListeCourseDTO}.
 */
@Mapper(componentModel = "spring")
public interface ListeCourseMapper extends EntityMapper<ListeCourseDTO, ListeCourse> {
    @Mapping(target = "listname", source = "listname", qualifiedByName = "clientId")
    ListeCourseDTO toDto(ListeCourse s);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);
}
