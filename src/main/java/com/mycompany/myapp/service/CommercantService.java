package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Commercant;
import com.mycompany.myapp.repository.CommercantRepository;
import com.mycompany.myapp.service.dto.CommercantDTO;
import com.mycompany.myapp.service.mapper.CommercantMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Commercant}.
 */
@Service
@Transactional
public class CommercantService {

    private final Logger log = LoggerFactory.getLogger(CommercantService.class);

    private final CommercantRepository commercantRepository;

    private final CommercantMapper commercantMapper;

    public CommercantService(CommercantRepository commercantRepository, CommercantMapper commercantMapper) {
        this.commercantRepository = commercantRepository;
        this.commercantMapper = commercantMapper;
    }

    /**
     * Save a commercant.
     *
     * @param commercantDTO the entity to save.
     * @return the persisted entity.
     */
    public CommercantDTO save(CommercantDTO commercantDTO) {
        log.debug("Request to save Commercant : {}", commercantDTO);
        Commercant commercant = commercantMapper.toEntity(commercantDTO);
        commercant = commercantRepository.save(commercant);
        return commercantMapper.toDto(commercant);
    }

    /**
     * Update a commercant.
     *
     * @param commercantDTO the entity to save.
     * @return the persisted entity.
     */
    public CommercantDTO update(CommercantDTO commercantDTO) {
        log.debug("Request to update Commercant : {}", commercantDTO);
        Commercant commercant = commercantMapper.toEntity(commercantDTO);
        commercant = commercantRepository.save(commercant);
        return commercantMapper.toDto(commercant);
    }

    /**
     * Partially update a commercant.
     *
     * @param commercantDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CommercantDTO> partialUpdate(CommercantDTO commercantDTO) {
        log.debug("Request to partially update Commercant : {}", commercantDTO);

        return commercantRepository
            .findById(commercantDTO.getId())
            .map(existingCommercant -> {
                commercantMapper.partialUpdate(existingCommercant, commercantDTO);

                return existingCommercant;
            })
            .map(commercantRepository::save)
            .map(commercantMapper::toDto);
    }

    /**
     * Get all the commercants.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CommercantDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Commercants");
        return commercantRepository.findAll(pageable).map(commercantMapper::toDto);
    }

    /**
     * Get all the commercants with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<CommercantDTO> findAllWithEagerRelationships(Pageable pageable) {
        return commercantRepository.findAllWithEagerRelationships(pageable).map(commercantMapper::toDto);
    }

    /**
     * Get one commercant by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CommercantDTO> findOne(Long id) {
        log.debug("Request to get Commercant : {}", id);
        return commercantRepository.findOneWithEagerRelationships(id).map(commercantMapper::toDto);
    }

    /**
     * Delete the commercant by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Commercant : {}", id);
        commercantRepository.deleteById(id);
    }
}
