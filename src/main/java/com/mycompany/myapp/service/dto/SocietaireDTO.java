package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Societaire} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SocietaireDTO implements Serializable {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String firstname;

    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String lastName;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String nomSociete;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String role;

    @NotNull
    @Min(value = 18L)
    @Max(value = 100L)
    private Long age;

    private CooperativeDTO coopname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNomSociete() {
        return nomSociete;
    }

    public void setNomSociete(String nomSociete) {
        this.nomSociete = nomSociete;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public CooperativeDTO getCoopname() {
        return coopname;
    }

    public void setCoopname(CooperativeDTO coopname) {
        this.coopname = coopname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SocietaireDTO)) {
            return false;
        }

        SocietaireDTO societaireDTO = (SocietaireDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, societaireDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SocietaireDTO{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", nomSociete='" + getNomSociete() + "'" +
            ", role='" + getRole() + "'" +
            ", age=" + getAge() +
            ", coopname=" + getCoopname() +
            "}";
    }
}
