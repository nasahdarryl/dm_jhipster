package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Livreurs} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class LivreursDTO implements Serializable {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String firstname;

    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String lastName;

    @NotNull
    private String vehicle;

    @NotNull
    @Min(value = 18L)
    @Max(value = 100L)
    private Long age;

    private ClientDTO clientfirstname;

    private SocietaireDTO socfirstname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public ClientDTO getClientfirstname() {
        return clientfirstname;
    }

    public void setClientfirstname(ClientDTO clientfirstname) {
        this.clientfirstname = clientfirstname;
    }

    public SocietaireDTO getSocfirstname() {
        return socfirstname;
    }

    public void setSocfirstname(SocietaireDTO socfirstname) {
        this.socfirstname = socfirstname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LivreursDTO)) {
            return false;
        }

        LivreursDTO livreursDTO = (LivreursDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, livreursDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LivreursDTO{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", vehicle='" + getVehicle() + "'" +
            ", age=" + getAge() +
            ", clientfirstname=" + getClientfirstname() +
            ", socfirstname=" + getSocfirstname() +
            "}";
    }
}
