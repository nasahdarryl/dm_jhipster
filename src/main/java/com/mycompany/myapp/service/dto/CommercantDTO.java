package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Commercant} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CommercantDTO implements Serializable {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String firstname;

    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String lastName;

    @NotNull
    @Min(value = 18L)
    @Max(value = 100L)
    private Long age;

    private String address;

    private SocietaireDTO socfirstname;

    private Set<ClientDTO> clients = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public SocietaireDTO getSocfirstname() {
        return socfirstname;
    }

    public void setSocfirstname(SocietaireDTO socfirstname) {
        this.socfirstname = socfirstname;
    }

    public Set<ClientDTO> getClients() {
        return clients;
    }

    public void setClients(Set<ClientDTO> clients) {
        this.clients = clients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommercantDTO)) {
            return false;
        }

        CommercantDTO commercantDTO = (CommercantDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commercantDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommercantDTO{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", age=" + getAge() +
            ", address='" + getAddress() + "'" +
            ", socfirstname=" + getSocfirstname() +
            ", clients=" + getClients() +
            "}";
    }
}
