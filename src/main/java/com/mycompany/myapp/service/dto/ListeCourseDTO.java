package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.enumeration.Status;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.ListeCourse} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ListeCourseDTO implements Serializable {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String name;

    @NotNull
    private LocalDate creationdate;

    @NotNull
    private LocalDate modificationdate;

    @NotNull
    private Long totalItems;

    @NotNull
    private Status status;

    private ClientDTO listname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(LocalDate creationdate) {
        this.creationdate = creationdate;
    }

    public LocalDate getModificationdate() {
        return modificationdate;
    }

    public void setModificationdate(LocalDate modificationdate) {
        this.modificationdate = modificationdate;
    }

    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ClientDTO getListname() {
        return listname;
    }

    public void setListname(ClientDTO listname) {
        this.listname = listname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ListeCourseDTO)) {
            return false;
        }

        ListeCourseDTO listeCourseDTO = (ListeCourseDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, listeCourseDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ListeCourseDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", creationdate='" + getCreationdate() + "'" +
            ", modificationdate='" + getModificationdate() + "'" +
            ", totalItems=" + getTotalItems() +
            ", status='" + getStatus() + "'" +
            ", listname=" + getListname() +
            "}";
    }
}
