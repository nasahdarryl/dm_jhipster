application {
  config {
    baseName Coopcycle
    applicationType monolith
    databaseType sql
    prodDatabaseType postgresql
    clientFramework react
    clientTheme darkly
    languages [en,fr,es,de,it]
    serverPort 8080
  }
  entities Client, Commercant, Livreurs, Societaire, Article, ListeCourse, Cooperative
  dto * with mapstruct
}


enum Status {
  COMPLETED, PROCESSING
}

entity Client {
  firstName String pattern(/^[A-Z][a-z]+\d$/) required
  lastName String pattern(/^[A-Z][a-z]+\d$/)
  age Long min(18) max(100) required
  address String
}
entity Commercant {
  firstname String pattern(/^[A-Z][a-z]+\d$/) required
  lastName String pattern(/^[A-Z][a-z]+\d$/)
  age Long min(18) max(100) required
  address String
}
entity Livreurs {
  firstname String pattern(/^[A-Z][a-z]+\d$/) required
  lastName String pattern(/^[A-Z][a-z]+\d$/)
  vehicle String required
  age Long min(18) max(100) required
}
entity Societaire {
  firstname String pattern(/^[A-Z][a-z]+\d$/) required
  lastName String pattern(/^[A-Z][a-z]+\d$/)
  nomSociete String pattern(/^[A-Z][a-z]+\d$/) required
  role String pattern(/^[A-Z][a-z]+\d$/) required
  age Long min(18) max(100) required
}
entity Article {
  name String pattern(/^[A-Z][a-z]+\d$/) required
  price Long required
}
entity ListeCourse {
  name String pattern(/^[A-Z][a-z]+\d$/) required
  creationdate LocalDate required
  modificationdate LocalDate required
  totalItems Long required
  status Status required
}

entity Cooperative {
  name String pattern(/^[A-Z][a-z]+\d$/) required
  location String pattern(/^[A-Z][a-z]+\d$/) required
}

relationship OneToOne {
    Livreurs{clientfirstname required} to Client
  Commercant{Socfirstname required} to Societaire
  Livreurs{Socfirstname required} to Societaire
  Client{Socfirstname required} to Societaire
}

relationship ManyToMany {
    Commercant to Client
  Article to ListeCourse
  Article to Commercant
}

relationship OneToMany {
  Client to ListeCourse{listname required}
}

relationship ManyToOne {
  Societaire{coopname required} to Cooperative {socname required}
}

paginate * with infinite-scroll
